.. _devel-installation:

============
Installation
============

Overview
========

.. _devel-downloading:

Downloading
-----------

| A Git_ repository is available on Gitlab_ at https://gitlab.com/Code_TYMPAN/code_tympan.
| A Gitflow workflow is currently used; so the stable branch is master, and the development branch is develop.

.. _Git: https://git-scm.com/
.. _Gitlab: https://gitlab.com/

The project also requires some third party dependencies (see below for
more details).

Those dependencies are divided into heavy, potentially system-wide,
dependencies and lighter dependencies which can easily be
bundled in a source package along with Code_TYMPAN.

Lightweight dependencies
------------------------

Those dependencies are to be downloaded as a companion source package
and unzipped into the ``3rdparty`` directory. Please refer to the
``3rdparty/README`` file which provides the exact URL for your current
version of Code_TYMPAN.

Major dependencies
------------------

The following dependencies are a bigger deal as their are likely to be
already installed in your development environment in a system-wide
way. If you are running a Linux box, the best way to get them is
through your distribution package manager. If you are running a
Windows box, you should run the dedicated installers.

More details are given in further sections, on an OS and variant basis.

Boost
        Boost is a set of C++ libraries which provide a higher level
        of expressiveness and convenience for C++. A version 1.70 or
        newer is recommended.

Qt 5
        Qt is a toolkit for developing Graphical User Interface and
        providing portable and commonly used facilities such as
        multi-threading or reading XML files... Version 5.14
        is recommended.

OpenGL
        is a common 3D rendering API. It is mainly accessed through
        the QtOpenGL components.

CMake
        CMake is a multi platform built tool used to build Code_TYMPAN
        (and CGAL and many other OpenSource projects by the way). A
        version 3.18 or newer is required.

CGAL
        CGAL is a Computational Geometry library and itself depends
        upon both Boost and Qt and as such needs to be installed in a
        compatible way with your Qt and Boost installation.
        CGAL is used in the Python code for altimetry mesh triangulation and refinement. It is also used
        in the C++ code (business part) for infrastructure polygons triangulation.

Python
        A Python interpreter is required, version 3.7.x

Cython
        Cython is a programming language inspired from Python, allowing to
        write Python code with C/C++ declarations. The cython code is then
        translated into C/C++ and compiled as a Python library. It is used in
        Code_TYMPAN to interface Python code with TYMPAN C++ libraries. A version
        0.29 or above is required.

Shapely
        Shapely is a Python package for manipulation and analysis of planar
        geometric objects. Version 1.7.1 is suggested.

SWIG
        Swig is a software development tool that connects programs written in
        C and C++ with a variety of high-level programming languages.
        It is needed by the 3rd party dependency *CGAL-bingings*, to connect
        C++ with Python code.


Documentation dependencies
--------------------------

The C++ source code is annotated and API documentation can be
extracted with Doxygen_ and Graphviz_ whereas this documentation is generated from
reStructuredText_ sources thanks to Sphinx_. The breathe_ extension
(shipped within the 3rd party archive) provides a bridge between the
two and MathJax_ is used to nicely display equations.

Doxygen_, Graphviz_ and Sphinx_ need to be installed either from your package
manager or from the official Windows installer. As there are no specific
difficulties, please refer to their official documentations.

.. _reStructuredText: https://docutils.sourceforge.io/rst.html
.. _Sphinx: https://www.sphinx-doc.org/en/master/
.. _Doxygen: https://www.doxygen.nl/index.html
.. _Graphviz: https://www.graphviz.org
.. _breathe: https://github.com/michaeljones/breathe/
.. _MathJax: https://www.mathjax.org/


Windows 10
==========

| This section deals with the installation on Windows 10 using the native Microsoft Visual Studio development
| environment, which is assumed to be already installed.
| At this time, the community edition can be used for free as Code_TYMPAN is an open-source project.
| It can be downloaded at https://visualstudio.microsoft.com/fr/thank-you-downloading-visual-studio/?sku=Community&rel=16
|
| **IMPORTANT** : At this time, Code_TYMPAN is compatible with C++ 2017, so you must have this toolset installed and selected
| on your Visual Studio environment.
| This corresponds with CMake variable -DCMAKE_GENERATOR_TOOLSET=v141
|
| Installation using MinGW / MSYS has not been attempted and is not supported for now.
| (note that this toolchain is supported by CMake but that Boost is **officially unsupported** on MinGW / MSYS) !

*NB* : Since 4.2, Code_TYMPAN targets 64 bit CPU architecture.

To install the development environment for Windows, here are the various installers
to download and execute.

Boost

        Binary versions of Boost are available on SourceForge.
        For instance, you can download the 64 bits 1.70.0 Boost version for Visual Studio 2017 toolchain at
        https://sourceforge.net/projects/boost/files/boost-binaries/1.70.0/boost_1_70_0-msvc-14.1-64.exe/download.
        Adapt version and toolchain to your needs.
        See `Installing Boost`_ for detailed instructions.

Qt 5

        Download the installer for the latest Qt 5 version and for
        your version of the toolchain (Visual Studio 2019)
        from : https://www.qt.io/download-qt-installer

CMake
        https://cmake.org/download/

CGAL
        See the `Compiling CGAL`_ (no real binary installer available on Windows) :
        https://github.com/CGAL/cgal/releases/download/releases%2FCGAL-4.14.3/CGAL-4.14.3-Setup.exe

Cython
        Cython can be installed in python virtual environment.
        See :ref:`install-python` for details.

Shapely
        Shapely can be installed in Python environment.
        See :ref:`install-python` for details.

SWIG
        A prebuilt executable of SWIG can be downloaded at
        http://prdownloads.sourceforge.net/swig/swigwin-4.0.1.zip
        Download and unzip here C:\\dists\\swigwin-4.0.1, for example.
        Add this folder path to the environment variable PATH.
        Add the following environment variables :

        .. code-block:: winbatch

          set SWIG_DIR=C:\dists\swigwin-4.0.1
          set SWIG_EXECUTABLE=C:\dists\swigwin-4.0.1\swig.exe
          set PATH=C:\dists\swigwin-4.0.1;%PATH%


.. _install-python:

Python environment installation
-------------------------------

| `Download <https://www.python.org/downloads/>`_ and install Python 3.7.x for instance to C:\\Python37.
| **Do not add anything to the PATH.**

Launch a command window, adapt and execute following commands to create a 3.7.x python virtual environment :

.. code-block:: winbatch

  c:
  cd \
  cd dists/python
  C:\Python37\python.exe -m venv venv37tympan

  # Activate virtual environment
  C:\dists\python\venv37tympan\Scripts\activate.bat

  # Update install tools
  pip install --upgrade pip setuptools wheel
  pip install cython==0.29.6 numpy==1.16.4 scipy==1.2.1 matplotlib==3.0.3 pandas==0.24.2 xlsxwriter==1.1.6 shapely 'sphinx>=3,<4' graphviz


Installing Boost
----------------
Boost is a mandatory dependency of CGAL. Binary versions of Boost are available
on `SourceForge <https://sourceforge.net/projects/boost/files/boost-binaries/>`_.
The Boost installers install both Boost headers and precompiled libraries.

The procedure below applies for 1.70.0 version of Boost and Visual C++ 2017 :

#. Download and run the file boost_1_70_0-msvc-XX.Y-64.exe (where XX.Y = 14.0 for VC 2015, XX.Y = 14.1 for VC 2017, XX.Y = 14.2 for VC 2019).
   Currently, we suggest to download this
   `file <https://sourceforge.net/projects/boost/files/boost-binaries/1.70.0/boost_1_70_0-msvc-14.1-64.exe/download>`_
#. Extract the files to a new directory, e.g. c:\\Boost\\boost_1_70_0.
#. Set the following two environment variables to point respectively to the path of the libraries and the headers

   .. code-block:: winbatch

     BOOST_LIBRARYDIR = C:\Boost\boost_1_70_0\lib64-msvc-14.1
     BOOST_INCLUDEDIR = C:\Boost\boost_1_70_0

This will help CMake to find Boost.


Compiling CGAL
--------------

Building and installing CGAL is mandatory in order to build Code_TYMPAN.

CGAL depends upon some libraries amongst which Boost and Qt or
build tool such as CMake which are supposed to have already been
installed. It also depends on GMP and MPFR which can
be installed through CGAL installer on Windows (see below).

The following procedure is adapted from CGAL documentation named
`Installing from the Source Archive <https://doc.cgal.org/latest/Manual/windows.html#install-from-source>`_

#. Download `CGAL-4.14.3-Setup.exe <https://github.com/CGAL/cgal/releases/download/releases%2FCGAL-4.14.3/CGAL-4.14.3-Setup.exe>`_
   and extract sources to C:\\dists\\CGAL-4.14.3, for instance.
#. At the end of extraction, install GMP and MPFR libraries.
#. Add manually C:\\dists\\CGAL-4.14.3\\auxiliary\\gmp\\lib to PATH environment variable.
#. In a command window, adapt and execute following commands :

   .. code-block:: winbatch

     mkdir C:\dists\build-CGAL-4.14.3
     mkdir C:\dists\install-CGAL-4.14.3
     cd build-CGAL-4.14.3
     cmake ..\CGAL-4.14.3 -G "Visual Studio 16 2019" -DCMAKE_GENERATOR_TOOLSET=v141 -DCMAKE_INSTALL_PREFIX=..\install-CGAL-4.14.3 -A x64
     cmake --build . --target install --config Debug
     cmake --build . --target install --config Release

#. Add environment variable CGAL_DIR with value C:\\dists\\install-CGAL-4.14.3


Building Code_TYMPAN
--------------------
Prerequisite : `Major dependencies`_ must be installed.

First clone git repository to obtain the sources.
In a Git Bash windows or a command windows with git accessible on the PATH :

.. code-block:: winbatch

  C:
  cd \
  mkdir projects
  cd projects
  git clone https://gitlab.com/Code_TYMPAN/code_tympan.git


Second, download and extract `Lightweight dependencies`_.

Third, check and adapt environment variables which should be set as follow :

.. code-block:: winbatch

  BOOST_INCLUDEDIR = C:\Boost\boost_1_70_0
  BOOST_LIBRARYDIR = C:\Boost\boost_1_70_0\lib64-msvc-14.1
  CGAL_DIR = C:\dists\install-CGAL-4.14.3
  PATH = C:\Qt\5.14.0\msvc2017_64\bin;C:\Qt\5.14.0\msvc2017_64\plugins\platforms;C:\dists\CGAL-4.14.3\auxiliary\gmp\lib;C:\dists\swigwin-4.0.1;C:\Program Files\doxygen\bin;C:\Program Files (x86)\Graphviz2.38\bin;C:\Boost\boost_1_70_0\lib64-msvc-14.1;%PATH%
  SWIG_DIR = C:\dists\swigwin-4.0.1
  SWIG_EXECUTABLE = C:\dists\swigwin-4.0.1\swig.exe

You can then build Code_TYMPAN in Release or Debug mode using CMake.

Release Build
^^^^^^^^^^^^^
Create build and install repositories :

.. code-block:: winbatch

  cd C:\projects
  mkdir code_tympan_build
  mkdir code_tympan_install

Build and install Code_TYMPAN using CMake :

.. code-block:: winbatch

  cd code_tympan_build
  # Activate python virtual environment
  C:\dists\python\venv37tympan\Scripts\activate.bat
  cmake ..\code_tympan -G "Visual Studio 16 2019" -DCMAKE_GENERATOR_TOOLSET=v141 -DCMAKE_BUILD_TYPE=Release -DCMAKE_PREFIX_PATH=C:\Qt\5.14.0\msvc2017_64\lib\cmake -DCMAKE_INSTALL_PREFIX=..\code_tympan_install -A x64 >> output_file.txt 2>&1
  # System build output written in output.txt file
  cmake --build . --config Release
  cmake --build . --target install --config Release

Debug Build
^^^^^^^^^^^
Debug and release system builds must be separated.
Create build and install repositories :

.. code-block:: winbatch

  cd C:\projects
  mkdir code_tympan_build_d
  mkdir code_tympan_install_d

Build and install Code_TYMPAN using CMake :

.. code-block:: winbatch

  cd code_tympan_build_d
  # Activate python virtual environment
  C:\dists\python\venv37tympan\Scripts\activate.bat
  cmake ..\code_tympan -G "Visual Studio 16 2019" -DCMAKE_GENERATOR_TOOLSET=v141 -DCMAKE_BUILD_TYPE=Debug -DCMAKE_PREFIX_PATH=C:\Qt\5.14.0\msvc2017_64\lib\cmake -DCMAKE_INSTALL_PREFIX=..\code_tympan_install_d -A x64 >> output_file.txt 2>&1
  # System build output written in output.txt file
  cmake --build . --config Debug
  cmake --build . --target install --config Debug


Running Tests
^^^^^^^^^^^^^
.. code-block:: winbatch

  cd code_tympan_build
  # Activate python virtual environment
  C:\dists\python\venv37tympan\Scripts\activate.bat
  SetEnvTympanTests.bat
  set TYMPAN_PYTHON_INTERP=C:\dists\python\venv37tympan\Scripts\python.exe
  ctest -C Release --output-on-failure


Build Documentation
^^^^^^^^^^^^^^^^^^^

.. code-block:: winbatch

  cd code_tympan_build
  # Activate python virtual environment
  C:\dists\python\venv37tympan\Scripts\activate.bat
  SetEnvTympanTests.bat
  # run doc target
  cmake --build . --target doc


Working with Visual Studio
^^^^^^^^^^^^^^^^^^^^^^^^^^
| To work in Release mode, open Release mode Solution at C:\\projects\\code_tympan_build\\Code_TYMPAN.sln,
| To work in Debug mode, open Debug mode Solution at C:\\projects\\code_tympan_build_d\\Code_TYMPAN.sln.

In Visual Studio, please check build mode (Debug / Release) is consistent
with the solution build mode.

.. note:: More about the CMake build system

  If you want to add components or otherwise change the build system
  for Code_TYMPAN you might want to have a look at :ref:`Build System
  Documentation <build-system>` first.


Ubuntu / Debian standard install
================================

This section introduces the installation from the package manager of a
Linux based distribution. It assumes a pretty recent version of the
system on the date of writing, so that most packages are available
from standard package repositories in a compatible version. Here we
use *Debian buster*.

You will need to install packages first, then the Code_Tympan third party package.
Build, install, running tests and documentation creation are described.

You will need to add sudo command before apt-get cause packages should be installed as administrator:

.. code-block:: sh

    # Install needed packages:
    sudo apt update
    sudo apt install -y software-properties-common
    sudo apt install -y cmake swig cython git
    sudo apt install -y qt5-default libqt5opengl5 libqt5opengl5-dev
    sudo apt install -y python3 python3-dev
    sudo apt install doxygen
    # virtual-env
    python3 -m venv env-CodeTympan
    . env-CodeTympan/bin/activate
    pip install --upgrade pip setuptools wheel
    pip install cython==0.29.6 numpy==1.16.4 scipy==1.2.1 matplotlib==3.0.3 pandas==0.24.2 xlsxwriter==1.1.6 shapely 'sphinx>=3,<4' graphviz
    pip install --upgrade pandas
    # Install CGAL
    mkdir CGAL
    cd CGAL
    wget https://github.com/CGAL/cgal/archive/refs/tags/v4.14.3.tar.gz
    tar xf v4.14.3.tar.gz
    cd cgal-4.14.3
    mkdir build
    cd build
    cmake ../
    make
    # Boost
    cd ../../../
    mkdir boost
    cd boost
    wget https://boostorg.jfrog.io/artifactory/main/release/1.70.0/source/boost_1_70_0.tar.gz
    tar xf boost_1_70_0.tar.gz
    # clone CodeTympan
    cd ..
    git clone https://gitlab.com/Code_TYMPAN/code_tympan
    # Third party install
    cd code_tympan/3rdparty
    wget https://sourceforge.net/projects/code-tympan/files/3rdparty/Code_TYMPAN-4.7_third-party-07.zip/download -O Code_TYMPAN-4.7_third-party-07.zip
    unzip Code_TYMPAN-4.7_third-party-07.zip
    # build Code Tympan
    cd ..
    mkdir build
    cd build
    cmake ../ -DCMAKE_INSTALL_PREFIX=../../code_tympan_install -DPYTHON_INCLUDE_DIR=/usr/include/python3.7m/ -DCMAKE_BUILD_TYPE:STRING=Release -DCGAL_DIR=../../CGAL/cgal-4.14.3/build -DPYTHON_EXECUTABLE=../../env-CodeTympan/bin/python -DBOOST_ROOT=../../boost/boost_1_70_0
    make -j4
    # tests
    ctest -C Release --output-on-failure
    # doc
    make doc
    # install
    make install
