.. _dev-conventions:

Conventions
===========

.. note::

   This are coding conventions for Code_TYMPAN.


C++ indent style
----------------

Allman Style. See https://en.wikipedia.org/wiki/Indentation_style#Allman_style

The formatting is checked by clang-format_. See
https://clang.llvm.org/docs/ClangFormat.html for instructions on how to
integrate it in your favourite editor.  It can also be executed from the
command line::

  $ clang-format -i <file>

.. note::
  Good news ! clang-format_ is already integrated in latest versions of Visual
  Studio.

.. warning::
  Some formatting rules need a version of clang-format greater than 11.  In
  some linux distributions, the ``clang-format`` command is bound to an old
  version of clang-format_ (version 7 on debian buster). You have to ensure
  that your editor can find a version >= 11 of clang-format.

Comments
--------

Use Doxygen_ to comment functions, classes, methods & attributes in header
files.


Include guards
--------------

Depend on the Tympan module and the filename.

.. code-block:: c++

   #ifndef TY_<MODULE_NAME>
   #define TY_<MODULE_NAME>

   // Source code here

   #endif // TY_<MODULE_NAME>

Former convention with wrapping double underscores is abandonned as this is the
standard library convention. "Regular" projects, even such as Boost and Eigen,
don't do that.


Naming
------

* underscore+lowercase files and directories (avoid linux/windows compatibility
  problems). Exception may be accepted when a file only contains a single class
  (java style), then the file name may be the same (case included) as the class
  name. Includes MUST match the case of the files.

* uppercase constants

* camelcase function, class, attribute and method names. Class name starts with
  an uppercase letter, other with lowercase.

* private/protected attribute and method's name starts with an underscore '_'
  (public doesn't)

* underscored variable names

For Python code, only class names are CamelCased, as well as other things
directly bound from the C++ code. Function, attribute and method names are
underscore_cased.

No more need for the `TY` prefix, use the `tympan` namespace.

`LP` prefix should still be used for Tympan's smart pointers (though its meaning
has been lost in byte paradise).



Misc
----

- UTF-8 coding
- No trailing whitespaces


.. _Doxygen: https://www.doxygen.nl/index.html
.. _clang-format: https://clang.llvm.org/docs/ClangFormat.html
