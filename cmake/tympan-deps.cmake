# Looks for Code TYMPAN dependencies
set(TYMPAN_3RDPARTY_DLL_DIRS "")

find_package(Qt5Core REQUIRED)
find_package(Qt5Gui REQUIRED)
find_package(Qt5OpenGL REQUIRED)
find_package(Qt5Xml REQUIRED)
find_package(Qt5Test REQUIRED)
find_package(Qt5PrintSupport REQUIRED)
find_package(Qt5Widgets REQUIRED)

include_directories(${Qt5Core_INCLUDES})
add_definitions(${Qt5Core_DEFINITIONS})

include_directories(${Qt5Gui_INCLUDES})
add_definitions(${Qt5Gui_DEFINITIONS})


include_directories(${Qt5Widgets_INCLUDES})
add_definitions(${Qt5Widgets_DEFINITIONS})

include_directories(${Qt5OpenGL_INCLUDES})
add_definitions(${Qt5OpenGL_DEFINITIONS})

include_directories(${Qt5Xml_INCLUDES})
add_definitions(${Qt5Xml_DEFINITIONS})

include_directories(${Qt5Test_INCLUDES})
add_definitions(${Qt5Test_DEFINITIONS})

include_directories(${Qt5PrintSupport_INCLUDES})
add_definitions(${Qt5PrintSupport_DEFINITIONS})

include(tympan-deps-boost-qt)
include(tympan-deps-cgal)


# Load the module to add an external project.
include (ExternalProject)
# Set default 'ExternalProject' root directory.
set_directory_properties(PROPERTIES EP_BASE ${PROJECT_BINARY_DIR}/3rdparty)

include(tympan-deps-gtest)
include(tympan-deps-dime)
include(tympan-deps-nmpb)
include(tympan-deps-cgalbindings)
include(tympan-deps-ply)

if(SYS_NATIVE_WIN)
  set(LD_VARNAME "PATH")
endif()
if(SYS_LINUX)
  set(LD_VARNAME "LD_LIBRARY_PATH")
endif()

build_native_path_list(TYMPAN_3RDPARTY_DLL_NATIVE_DIRS "${TYMPAN_3RDPARTY_DLL_DIRS}")

message(STATUS "You will have to ensure that the following directories "
  "are searched for shared libratries / DLL when launching the program : "
  "${TYMPAN_3RDPARTY_DLL_NATIVE_DIRS}")

