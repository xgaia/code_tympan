# Simple Boost - Qt incompatibility work-around :
# There is some bad interaction between Qt and Boost around the token 'foreach'
# https://svn.boost.org/trac/boost/ticket/6455
# add_definitions(-DQT_NO_KEYWORDS) # This is even worse
#
# The following define prevents Qt from defining the `foreach` macro
# which is a source of problem because boost uses the `foreach` as
# namespace identifier without interfering any further.
add_definitions(-Dforeach=foreach)


# Find and configure for using Qt
set(CMAKE_AUTOMOC ON)
set(CMAKE_AUTOUIC ON)
set(CMAKE_INCLUDE_CURRENT_DIR ON)

find_package(Qt5 COMPONENTS Core Gui OpenGL Xml Test PrintSupport Widgets REQUIRED)

include_directories(${Qt5Core_INCLUDES})
add_definitions(${Qt5Core_DEFINITIONS})

include_directories(${Qt5Gui_INCLUDES})
add_definitions(${Qt5Gui_DEFINITIONS})


include_directories(${Qt5Widgets_INCLUDES})
add_definitions(${Qt5Widgets_DEFINITIONS})

include_directories(${Qt5OpenGL_INCLUDES})
add_definitions(${Qt5OpenGL_DEFINITIONS})

include_directories(${Qt5Xml_INCLUDES})
add_definitions(${Qt5Xml_DEFINITIONS})

include_directories(${Qt5Test_INCLUDES})
add_definitions(${Qt5Test_DEFINITIONS})

include_directories(${Qt5PrintSupport_INCLUDES})
add_definitions(${Qt5PrintSupport_DEFINITIONS})


if(SYS_NATIVE_WIN)
  list(APPEND TYMPAN_3RDPARTY_DLL_DIRS  ${QT_INCLUDES})
  list(APPEND TYMPAN_3RDPARTY_DLL_DIRS  ${QT_LIBRARIES})
  
endif()
if(SYS_LINUX)
  list(APPEND TYMPAN_3RDPARTY_DLL_DIRS  ${QT_LIBRARY_DIR})
endif()

#set(Boost_DEBUG TRUE) # Activate me to debug problems finding Boost
set(Boost_DETAILED_FAILURE_MESSAGE TRUE)
# Find Boost
set(Boost_ADDITIONAL_VERSIONS "1.46" "1.46.1" "1.49" "1.49.0" "1.67.0")
#set(Boost_USE_MULTITHREADED ON) # You might need to activate me later
find_package(Boost COMPONENTS thread regex REQUIRED)
include_directories(${Boost_INCLUDE_DIRS})

# This is useless (but harmless) in case we link statically with Boost.
list(APPEND TYMPAN_3RDPARTY_DLL_DIRS  ${Boost_LIBRARY_DIRS})

find_package(OpenGL REQUIRED) # XXX Shouldn't this be implied by finding QtOpenGL ?
include_directories(${OPENGL_INCLUDE_DIR})

if(TYMPAN_DEBUG_CMAKE)
message(STATUS "TYMPAN_DEBUG: Boost_INCLUDE_DIRS: " "${Boost_INCLUDE_DIRS}")
message(STATUS "TYMPAN_DEBUG: Boost_LIBRARIES   : " "${Boost_LIBRARIES}")
message(STATUS "TYMPAN_DEBUG: QT_INCLUIDES      : " ${QT_INCLUDES})
message(STATUS "TYMPAN_DEBUG: QT_LIBRARIES      : " ${QT_LIBRARIES})
message(STATUS "TYMPAN_DEBUG: QT_BINARY_DIR      : " ${QT_BINARY_DIR})
message(STATUS "TYMPAN_DEBUG: Qt5Xml_INCLUDES      : " ${Qt5Xml_INCLUDES})
message(STATUS "TYMPAN_DEBUG: OPENGL_LIBRARIES  : " ${OPENGL_LIBRARIES})
endif()
