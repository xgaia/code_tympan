# Add the Google Test project as an external project.
ExternalProject_Add(GTest
  URL ${TYMPAN_3RDPARTY_GTEST}
  URL_MD5 ${TYMPAN_3RDPARTY_GTEST_MD5}
  # Configuration step. GTest doc said "Use shared (DLL) run-time lib even when
  # Google Test is built as static lib."
  CMAKE_ARGS -Dgtest_force_shared_crt=ON  
  INSTALL_COMMAND ""
  BUILD_IN_SOURCE 0
)

ExternalProject_Get_Property(GTest SOURCE_DIR BINARY_DIR)
set(GTEST_SOURCE_DIR ${SOURCE_DIR})
set(GTEST_BINARY_DIR ${BINARY_DIR})
# Force to be an include directory to avoid warnings on a project you have no control over
#set(GTEST_INCLUDE_DIR ${GTEST_SOURCE_DIR}/include)
include_directories(SYSTEM ${GTEST_SOURCE_DIR}/include)

message(STATUS ${CMAKE_BUILD_TYPE})
if(CMAKE_BUILD_TYPE MATCHES "Debug")
    add_custom_command(
        TARGET GTest POST_BUILD
        COMMAND ${CMAKE_COMMAND} -E copy ${BINARY_DIR}/Debug/gtestd.lib ${BINARY_DIR}/Debug/gtest.lib)
    add_custom_command(
        TARGET GTest POST_BUILD
        COMMAND ${CMAKE_COMMAND} -E copy ${BINARY_DIR}/Debug/gtest_maind.lib ${BINARY_DIR}/Debug/gtest_main.lib)
endif()

if(TYMPAN_DEBUG_CMAKE)
message(STATUS "INFO GTEST_SOURCE_DIR: " "${GTEST_SOURCE_DIR}" )
message(STATUS "INFO GTEST_BINARY_DIR: " "${GTEST_BINARY_DIR}" )
message(STATUS "INFO GTEST_INCLUDE_DIR: " "${GTEST_INCLUDE_DIR}" )
endif()
