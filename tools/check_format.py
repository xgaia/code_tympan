"""Check that all source and header files respect clang-format"""
from pathlib import Path
from subprocess import check_output


def all_source_files():
    sources_dir = Path(__file__).parent.parent / "Tympan"
    h_files = sources_dir.glob("**/*.h")
    hpp_files = sources_dir.glob("**/*.hpp")
    cpp_files = sources_dir.glob("**/*.cpp")
    return [*h_files, *hpp_files, *cpp_files]


def file_is_well_formatted(source_file):
    output = check_output(['clang-format', str(source_file),
                           '-output-replacements-xml'])
    return 'replacement offset' not in str(output)


def get_invalid_files():
    invalid_files = []
    for source_file in all_source_files():
        if not file_is_well_formatted(source_file):
            invalid_files.append(source_file)
    return invalid_files


invalid_files = get_invalid_files()
if invalid_files:
    print("Some files are not formatted correctly:")
    for invalid_file in invalid_files:
        print(invalid_file)
    raise SystemExit(1)
print("Congratulation: Your files are well formatted!")
