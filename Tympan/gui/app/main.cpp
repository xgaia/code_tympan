/*
 * Copyright (C) <2012> <EDF-R&D> <FRANCE>
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

/**
 * \file main.cpp
 * \brief fichier principal de lancement de l'application Tympan en mode IHM
 */

#include <qmessagebox.h>
#include <QDir>

#include "Tympan/core/logging.h"
#include "Tympan/models/business/init_registry.h"
#include "TYApplication.h"

static QtMessageHandler old_handler;

static void MyQTMessageHandler(QtMsgType type, const QMessageLogContext& context, const QString& message)
{
    if (old_handler != NULL)
    {
        old_handler(type, context, message);
    }

    switch (type)
    {
        case QtDebugMsg:
            break;
        case QtWarningMsg:
            break;
        case QtCriticalMsg:
        case QtFatalMsg:
            int selected;

            selected = QMessageBox::critical(NULL, "Attention !", message, "Debug", "Continue", "Quit");

            if (selected == 0)
            {
                // Debug
#if defined(WIN32)
                _CrtDbgBreak();
#endif
            }
            else if (selected == 1)
            {
                // Continue
            }
            else if (selected == 2)
            {
                // Quit
                exit(1);
            }
            break;
    }
}

static int tyMain(int argc, char** argv)
{
    bool success = false;

    old_handler = qInstallMessageHandler(MyQTMessageHandler);

    TYApplication tyApp(argc, argv);
    if (tyApp.init())
    {
        // Lance la boucle principal
        success = tyApp.run();

        // Termine correctement l'appli
        tyApp.close();
    }
    OMessageManager::get()->debug("Counters : TYElements created %u, deleted %u and ID generated %u",
                                  TYElement::getConstructorCount(), TYElement::getDestructorCount(),
                                  TYElement::getIdGenerationCount());
    qInstallMessageHandler(old_handler);

    // return code
    int ret;
    success ? ret = 0 : ret = 1;

    return ret;
}

bool setenv(const char* pVarEnvName, QString& pVarEnvValue, bool pForceUpdate)
{
    bool ret = true;
    QString currentVarEnvValue = QString::fromLocal8Bit(qgetenv(pVarEnvName));
    if (currentVarEnvValue != "" && !pForceUpdate)
    {
        OMessageManager::get()->info("Environment variable %s already set to : '%s'.\n", pVarEnvName,
                                     currentVarEnvValue.toUtf8().data());
        pVarEnvValue = currentVarEnvValue;
    }
    else
    {
        OMessageManager::get()->info("Trying to set environment variable %s to : '%s'.\n", pVarEnvName,
                                     pVarEnvValue.toUtf8().data());
        ret = qputenv(pVarEnvName, pVarEnvValue.toUtf8());
    }
    return ret;
}

bool setenv()
{
    bool ret = true;
    const QChar SEP = QDir::listSeparator();
#ifdef _DEBUG
    const char* PLUGINS = "/pluginsd";
    const char* CYTHON = "/cython_d";
    const bool FORCE_PATH_UPDATE = false;
#else
    const char* PLUGINS = "/plugins";
    const char* CYTHON = "/cython";
    const bool FORCE_PATH_UPDATE = true;
#endif

    const QString TYMPAN_INSTALL_DIR = QDir::currentPath();
    OMessageManager::get()->info("TYMPAN_INSTALL_DIR is '%s'.\n", TYMPAN_INSTALL_DIR.toUtf8().data());

    ret |= qputenv("PYTHONIOENCODING", "UTF8");

    QString PYTHONTYMPAN = TYMPAN_INSTALL_DIR + QString("/Python37");
    ret |= setenv("PYTHONTYMPAN", PYTHONTYMPAN, false);

    QString TYMPAN_SOLVERDIR = TYMPAN_INSTALL_DIR + QString(PLUGINS);
    ret |= setenv("TYMPAN_SOLVERDIR", TYMPAN_SOLVERDIR, false);

    QString CGAL_BINDINGS_PATH = TYMPAN_INSTALL_DIR + CYTHON + QString("/CGAL");
    ret |= setenv("CGAL_BINDINGS_PATH", CGAL_BINDINGS_PATH, false);

    QString TYMPAN_PYTHON_INTERP = PYTHONTYMPAN + QString("/python.exe");
    ret |= setenv("TYMPAN_PYTHON_INTERP", TYMPAN_PYTHON_INTERP, false);

    QString PYTHONPATH = TYMPAN_INSTALL_DIR + CYTHON + SEP + PYTHONTYMPAN + SEP + PYTHONTYMPAN + "/DLLs" +
                         SEP + PYTHONTYMPAN + "/Lib" + SEP + PYTHONTYMPAN + "/Lib/site-packages" + SEP +
                         TYMPAN_INSTALL_DIR;

    ret |= setenv("PYTHONPATH", PYTHONPATH, false);

    QString PATH = TYMPAN_INSTALL_DIR + SEP + PYTHONTYMPAN + SEP + PYTHONTYMPAN + "/DLLs" + SEP +
                   PYTHONTYMPAN + "/Lib" + SEP + PYTHONTYMPAN + "/Scripts";
    ret |= setenv("PATH", PATH, true);

    QString LD_LIBRARY_PATH = TYMPAN_INSTALL_DIR + "/lib" + SEP + TYMPAN_INSTALL_DIR + CYTHON + SEP +
                              TYMPAN_INSTALL_DIR + CYTHON + QString("/CGAL");
    ret |= setenv("LD_LIBRARY_PATH", LD_LIBRARY_PATH, false);

    return ret;
}

int main(int argc, char** argv)
{
    int ret = 0;
    bool ret_setenv = true;
    ret_setenv = setenv();
    if (!ret_setenv)
    {
        OMessageManager::get()->info("Unable to set environment variables, closing application.");
        return ret;
    }
    // Register TY* classes before starting the application
    tympan::init_registry();
    // Appel le main de Tympan
    ret = tyMain(argc, argv);
    return ret;
}