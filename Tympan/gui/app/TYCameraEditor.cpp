/*
 * Copyright (C) <2012> <EDF-R&D> <FRANCE>
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

/**
 * \file TYCameraEditor.cpp
 * \brief gestion de l'edition de la camera
 */

#include <qtimer.h>
#include <qcursor.h>

#include "Tympan/models/common/3d.h"
#include "Tympan/models/business/TYDefines.h"
#include "Tympan/models/business/TYElement.h"
#include "Tympan/models/business/TYPreferenceManager.h"
#include "Tympan/gui/tools/OGLCamera.h"
#include "Tympan/gui/app/TYRenderWindowInteractor.h"
#include "Tympan/gui/app/TYModelerFrame.h"
#include "TYCameraEditor.h"

using namespace Qt;

TYCameraEditor::TYCameraEditor(TYModelerFrame* pModeler) : TYAbstractSceneEditor(pModeler)
{
    _firstCall = true;

    setLeftButtonFunction(&TYCameraEditor::doNothing);
    setMiddleButtonFunction(&TYCameraEditor::doNothing);
    setRightButtonFunction(&TYCameraEditor::doNothing);
    setShiftLeftButtonFunction(&TYCameraEditor::doNothing);
    setShiftRightButtonFunction(&TYCameraEditor::doNothing);

    setLeftButtonFunction2D(&TYCameraEditor::cameraTranslate);
    setMiddleButtonFunction2D(&TYCameraEditor::cameraTranslate);
    setRightButtonFunction2D(&TYCameraEditor::cameraZoom);
    setShiftLeftButtonFunction2D(&TYCameraEditor::cameraTranslate);
    setShiftRightButtonFunction2D(&TYCameraEditor::cameraZoom);

    setLeftButtonFunction3D(&TYCameraEditor::cameraRotate);
    setMiddleButtonFunction3D(&TYCameraEditor::cameraTranslate);
    setRightButtonFunction3D(&TYCameraEditor::cameraZoom);
    setShiftLeftButtonFunction3D(&TYCameraEditor::cameraTranslate);
    setShiftRightButtonFunction3D(&TYCameraEditor::cameraZoom);

    setLeftButtonFunctionFree(&TYCameraEditor::cameraRotate);
    setMiddleButtonFunctionFree(&TYCameraEditor::doNothing);
    setRightButtonFunctionFree(&TYCameraEditor::doNothing);
    setShiftLeftButtonFunctionFree(&TYCameraEditor::doNothing);
    setShiftRightButtonFunctionFree(&TYCameraEditor::doNothing);

    _mouseEventActive = false;
    _keyEventActive = 0;
    setInteractionTime(50);
    setSensitivity(10.0);
    setWheelStep(1.0);

    _bKeyUp = false;
    _bKeyDown = false;
    _bKeyLeft = false;
    _bKeyRight = false;
    _bKeyPageUp = false;
    _bKeyPageDown = false;
    _bKeyShift = false;

    _pStepTimer = new QTimer(this);
    Q_CHECK_PTR(_pStepTimer);

    _pCurrentCamera = _pInteractor->getRenderer()->getActiveCamera();
    Q_CHECK_PTR(_pCurrentCamera);

    _zoomStep = 2.0f;
    if (TYPreferenceManager::exists(TYDIRPREFERENCEMANAGER, "CameraZoomStep"))
    {
        _zoomStep = TYPreferenceManager::getFloat(TYDIRPREFERENCEMANAGER, "CameraZoomStep");
    }
    else
    {
        TYPreferenceManager::setFloat(TYDIRPREFERENCEMANAGER, "CameraZoomStep", _zoomStep);
    }

    _translateStep = 5.0f;
    if (TYPreferenceManager::exists(TYDIRPREFERENCEMANAGER, "CameraTranslateStep"))
    {
        _translateStep = TYPreferenceManager::getFloat(TYDIRPREFERENCEMANAGER, "CameraTranslateStep");
    }
    else
    {
        TYPreferenceManager::setFloat(TYDIRPREFERENCEMANAGER, "CameraTranslateStep", _translateStep);
    }

    _rotateStep = 10.0f;
    if (TYPreferenceManager::exists(TYDIRPREFERENCEMANAGER, "CameraRotateStep"))
    {
        _rotateStep = TYPreferenceManager::getFloat(TYDIRPREFERENCEMANAGER, "CameraRotateStep");
    }
    else
    {
        TYPreferenceManager::setFloat(TYDIRPREFERENCEMANAGER, "CameraRotateStep", _rotateStep);
    }

    // Pour la mise a jour de l'orientation et la position des axes et de la grille
    QObject::connect(this, &TYCameraEditor::cameraUpdated, pModeler, &TYModelerFrame::updateAxes);
    QObject::connect(this, &TYCameraEditor::cameraUpdated, pModeler, &TYModelerFrame::updateGrid);
    // Pour le passage en mode wireframe lors du deplacement de la cam si l'option est active
    QObject::connect(this, &TYCameraEditor::startMovingCamera, pModeler,
                     &TYModelerFrame::startMovingRenderMode);
    QObject::connect(this, &TYCameraEditor::stopMovingCamera, pModeler,
                     &TYModelerFrame::stopMovingRenderMode);
    // L'echelle est mise a jour a la fin de chaque deplacement de camera
    QObject::connect(this, &TYCameraEditor::stopMovingCamera, pModeler, &TYModelerFrame::updateScale);

    setNavigationOnViewType(pModeler->getCurrentView());
}

TYCameraEditor::~TYCameraEditor() {}

void TYCameraEditor::setLeftButtonFunction(void (TYCameraEditor::*function)())
{
    if (function == NULL)
    {
        _leftButtonFunction = &TYCameraEditor::doNothing;
    }
    else
    {
        _leftButtonFunction = function;
    }
}

void TYCameraEditor::setMiddleButtonFunction(void (TYCameraEditor::*function)())
{
    if (function == NULL)
    {
        _middleButtonFunction = &TYCameraEditor::doNothing;
    }
    else
    {
        _middleButtonFunction = function;
    }
}

void TYCameraEditor::setRightButtonFunction(void (TYCameraEditor::*function)())
{
    if (function == NULL)
    {
        _rightButtonFunction = &TYCameraEditor::doNothing;
    }
    else
    {
        _rightButtonFunction = function;
    }
}

void TYCameraEditor::setShiftLeftButtonFunction(void (TYCameraEditor::*function)())
{
    if (function == NULL)
    {
        _shiftLeftButtonFunction = &TYCameraEditor::doNothing;
    }
    else
    {
        _shiftLeftButtonFunction = function;
    }
}

void TYCameraEditor::setShiftRightButtonFunction(void (TYCameraEditor::*function)())
{
    if (function == NULL)
    {
        _shiftRightButtonFunction = &TYCameraEditor::doNothing;
    }
    else
    {
        _shiftRightButtonFunction = function;
    }
}

void TYCameraEditor::setLeftButtonFunction2D(void (TYCameraEditor::*function)())
{
    if (function == NULL)
    {
        _leftButtonFunction2D = &TYCameraEditor::doNothing;
    }
    else
    {
        _leftButtonFunction2D = function;
    }
}

void TYCameraEditor::setMiddleButtonFunction2D(void (TYCameraEditor::*function)())
{
    if (function == NULL)
    {
        _middleButtonFunction2D = &TYCameraEditor::doNothing;
    }
    else
    {
        _middleButtonFunction2D = function;
    }
}

void TYCameraEditor::setRightButtonFunction2D(void (TYCameraEditor::*function)())
{
    if (function == NULL)
    {
        _rightButtonFunction2D = &TYCameraEditor::doNothing;
    }
    else
    {
        _rightButtonFunction2D = function;
    }
}

void TYCameraEditor::setShiftLeftButtonFunction2D(void (TYCameraEditor::*function)())
{
    if (function == NULL)
    {
        _shiftLeftButtonFunction2D = &TYCameraEditor::doNothing;
    }
    else
    {
        _shiftLeftButtonFunction2D = function;
    }
}

void TYCameraEditor::setShiftRightButtonFunction2D(void (TYCameraEditor::*function)())
{
    if (function == NULL)
    {
        _shiftRightButtonFunction2D = &TYCameraEditor::doNothing;
    }
    else
    {
        _shiftRightButtonFunction2D = function;
    }
}

void TYCameraEditor::setLeftButtonFunction3D(void (TYCameraEditor::*function)())
{
    if (function == NULL)
    {
        _leftButtonFunction3D = &TYCameraEditor::doNothing;
    }
    else
    {
        _leftButtonFunction3D = function;
    }
}

void TYCameraEditor::setMiddleButtonFunction3D(void (TYCameraEditor::*function)())
{
    if (function == NULL)
    {
        _middleButtonFunction3D = &TYCameraEditor::doNothing;
    }
    else
    {
        _middleButtonFunction3D = function;
    }
}

void TYCameraEditor::setRightButtonFunction3D(void (TYCameraEditor::*function)())
{
    if (function == NULL)
    {
        _rightButtonFunction3D = &TYCameraEditor::doNothing;
    }
    else
    {
        _rightButtonFunction3D = function;
    }
}

void TYCameraEditor::setShiftLeftButtonFunction3D(void (TYCameraEditor::*function)())
{
    if (function == NULL)
    {
        _shiftLeftButtonFunction3D = &TYCameraEditor::doNothing;
    }
    else
    {
        _shiftLeftButtonFunction3D = function;
    }
}

void TYCameraEditor::setShiftRightButtonFunction3D(void (TYCameraEditor::*function)())
{
    if (function == NULL)
    {
        _shiftRightButtonFunction3D = &TYCameraEditor::doNothing;
    }
    else
    {
        _shiftRightButtonFunction3D = function;
    }
}

void TYCameraEditor::setLeftButtonFunctionFree(void (TYCameraEditor::*function)())
{
    if (function == NULL)
    {
        _leftButtonFunctionFree = &TYCameraEditor::doNothing;
    }
    else
    {
        _leftButtonFunctionFree = function;
    }
}

void TYCameraEditor::setMiddleButtonFunctionFree(void (TYCameraEditor::*function)())
{
    if (function == NULL)
    {
        _middleButtonFunctionFree = &TYCameraEditor::doNothing;
    }
    else
    {
        _middleButtonFunctionFree = function;
    }
}

void TYCameraEditor::setRightButtonFunctionFree(void (TYCameraEditor::*function)())
{
    if (function == NULL)
    {
        _rightButtonFunctionFree = &TYCameraEditor::doNothing;
    }
    else
    {
        _rightButtonFunctionFree = function;
    }
}

void TYCameraEditor::setShiftLeftButtonFunctionFree(void (TYCameraEditor::*function)())
{
    if (function == NULL)
    {
        _shiftLeftButtonFunctionFree = &TYCameraEditor::doNothing;
    }
    else
    {
        _shiftLeftButtonFunctionFree = function;
    }
}

void TYCameraEditor::setShiftRightButtonFunctionFree(void (TYCameraEditor::*function)())
{
    if (function == NULL)
    {
        _shiftRightButtonFunctionFree = &TYCameraEditor::doNothing;
    }
    else
    {
        _shiftRightButtonFunctionFree = function;
    }
}

void TYCameraEditor::slotKeyPressed(int key)
{
    if (_pModeler->getCurrentView() == TYModelerFrame::FreeView)
    {
        // Update la cam courante
        _pCurrentCamera = _pInteractor->getRenderer()->getActiveCamera();
        switch (key)
        {
            case Qt::Key_Shift:
            {
                _bKeyShift = true;
                _keyEventActive++;
                if (_keyEventActive > 1)
                {
                    return;
                }
                else
                {
                    QObject::connect(_pStepTimer, &QTimer::timeout, this,
                                     &TYCameraEditor::cameraTranslateKey);
                }
            }
            break;
            case Qt::Key_Up:
            {
                _bKeyUp = true;
                _keyEventActive++;
                if (_keyEventActive > 1)
                {
                    return;
                }
                else
                {
                    QObject::connect(_pStepTimer, &QTimer::timeout, this,
                                     &TYCameraEditor::cameraTranslateKey);
                }
            }
            break;
            case Qt::Key_Down:
            {
                _bKeyDown = true;
                _keyEventActive++;
                if (_keyEventActive > 1)
                {
                    return;
                }
                else
                {
                    QObject::connect(_pStepTimer, &QTimer::timeout, this,
                                     &TYCameraEditor::cameraTranslateKey);
                }
            }
            break;
            case Qt::Key_Left:
            {
                _bKeyLeft = true;
                _keyEventActive++;
                if (_keyEventActive > 1)
                {
                    return;
                }
                else
                {
                    QObject::connect(_pStepTimer, &QTimer::timeout, this,
                                     &TYCameraEditor::cameraTranslateKey);
                }
            }
            break;
            case Qt::Key_Right:
            {
                _bKeyRight = true;
                _keyEventActive++;
                if (_keyEventActive > 1)
                {
                    return;
                }
                else
                {
                    QObject::connect(_pStepTimer, &QTimer::timeout, this,
                                     &TYCameraEditor::cameraTranslateKey);
                }
            }
            break;
            case Qt::Key_PageUp:
            {
                _bKeyPageUp = true;
                _keyEventActive++;
                if (_keyEventActive > 1)
                {
                    return;
                }
                else
                {
                    QObject::connect(_pStepTimer, &QTimer::timeout, this,
                                     &TYCameraEditor::cameraTranslateKey);
                }
            }
            break;
            case Qt::Key_PageDown:
            {
                _bKeyPageDown = true;
                _keyEventActive++;
                if (_keyEventActive > 1)
                {
                    return;
                }
                else
                {
                    QObject::connect(_pStepTimer, &QTimer::timeout, this,
                                     &TYCameraEditor::cameraTranslateKey);
                }
            }
            break;
        }

        if (_keyEventActive > 0)
        {
            _firstCall = true;
            _pStepTimer->start(_interactionTime);
            emit startMovingCamera();
        }
    }
}

void TYCameraEditor::slotKeyReleased(int key)
{
    if (_pModeler->getCurrentView() == TYModelerFrame::FreeView)
    {
        if (_keyEventActive > 0)
        {
            _keyEventActive--;
            switch (key)
            {
                case Qt::Key_Shift:
                {
                    _bKeyShift = false;
                }
                break;
                case Qt::Key_Up:
                {
                    _bKeyUp = false;
                }
                break;
                case Qt::Key_Down:
                {
                    _bKeyDown = false;
                }
                break;
                case Qt::Key_Left:
                {
                    _bKeyLeft = false;
                }
                break;
                case Qt::Key_Right:
                {
                    _bKeyRight = false;
                }
                break;
                case Qt::Key_PageUp:
                {
                    _bKeyPageUp = false;
                }
                break;
                case Qt::Key_PageDown:
                {
                    _bKeyPageDown = false;
                }
                break;
            }

            if (_keyEventActive == 0)
            {
                if (!_mouseEventActive)
                {
                    _pStepTimer->stop();
                    QObject::disconnect(_pStepTimer, 0, this, 0);

                    emit stopMovingCamera();
                }
            }
        }
    }
}

void TYCameraEditor::slotMousePressed(int x, int y, Qt::MouseButton button, Qt::KeyboardModifiers state)
{
    if (_mouseEventActive)
    {
        return;
    }

    // Update la cam courante
    _pCurrentCamera = _pInteractor->getRenderer()->getActiveCamera();

    if (_pModeler->getCurrentView() == TYModelerFrame::FreeView)
    {
        _pCurrentCamera->setDistanceStep(5, 5, 5);
    }
    else
    {
        _pCurrentCamera->setDistanceStep(30, 70, 30);
    }

    if (button & LeftButton)
    {
        // Left mouse button
        _mouseEventActive = true;

        if (state & ShiftModifier)
        {
            // With Shift button
            QObject::connect(_pStepTimer, &QTimer::timeout, this, _shiftLeftButtonFunction);
        }
        else
        {
            // Without shift button
            QObject::connect(_pStepTimer, &QTimer::timeout, this, _leftButtonFunction);
        }
    }
    else if (button & MidButton)
    {
        // Middle mouse button
        _mouseEventActive = true;
        QObject::connect(_pStepTimer, &QTimer::timeout, this, _middleButtonFunction);
    }
    else if (button & RightButton)
    {
        // Right mouse button
        _mouseEventActive = true;

        if (state & ShiftModifier)
        {
            // With Shift button
            QObject::connect(_pStepTimer, &QTimer::timeout, this, _shiftRightButtonFunction);
        }
        else
        {
            // Without Shift button
            QObject::connect(_pStepTimer, &QTimer::timeout, this, _rightButtonFunction);
        }
    }

    if (_mouseEventActive)
    {
        _currentMousePos[0] = x;
        _currentMousePos[1] = y;
        _lastMousePos[0] = x;
        _lastMousePos[1] = y;
        _firstCall = true;
        _pStepTimer->start(_interactionTime);

        emit startMovingCamera();
    }
}

void TYCameraEditor::slotMouseReleased(int x, int y, Qt::MouseButton button, Qt::KeyboardModifiers state)
{
    if (_mouseEventActive)
    {
        _mouseEventActive = false;
        if (_keyEventActive == 0)
        {
            _pStepTimer->stop();
            QObject::disconnect(_pStepTimer, 0, this, 0);

            emit stopMovingCamera();
        }
    }
}

void TYCameraEditor::slotMouseMoved(int x, int y, Qt::MouseButtons button, Qt::KeyboardModifiers state)
{
    _currentMousePos[0] = x;
    _currentMousePos[1] = y;
}

void TYCameraEditor::slotWheeled(int x, int y, int delta, Qt::KeyboardModifiers state)
{
    if (_pModeler->getCurrentView() != TYModelerFrame::FreeView)
    {
        // Update la cam courante
        _pCurrentCamera = _pInteractor->getRenderer()->getActiveCamera();
        _pCurrentCamera->setDistanceStep(30, 70, 30);

        _currentMousePos[0] = x;
        _currentMousePos[1] = y;

        _lastMousePos[0] = _currentMousePos[0] + int(delta * _wheelStep);
        _lastMousePos[1] = _currentMousePos[1] + int(delta * _wheelStep);

        // Update la cam courante
        _pCurrentCamera = _pInteractor->getRenderer()->getActiveCamera();

        cameraZoom();

        emit stopMovingCamera();
    }
}

void TYCameraEditor::slotViewTypeChanged(int view)
{
    setNavigationOnViewType(view);
}

void TYCameraEditor::setNavigationOnViewType(int view)
{
    if (view == TYModelerFrame::PerspView)
    {
        setToNavigation3D();
    }
    else if (view == TYModelerFrame::FreeView)
    {
        setToNavigationFree();
    }
    else
    {
        setToNavigation2D();
    }
}

void TYCameraEditor::setToNavigation2D()
{
    setLeftButtonFunction(_leftButtonFunction2D);
    setMiddleButtonFunction(_middleButtonFunction2D);
    setRightButtonFunction(_rightButtonFunction2D);
    setShiftLeftButtonFunction(_shiftLeftButtonFunction2D);
    setShiftRightButtonFunction(_shiftRightButtonFunction2D);
}

void TYCameraEditor::setToNavigation3D()
{
    setLeftButtonFunction(_leftButtonFunction3D);
    setMiddleButtonFunction(_middleButtonFunction3D);
    setRightButtonFunction(_rightButtonFunction3D);
    setShiftLeftButtonFunction(_shiftLeftButtonFunction3D);
    setShiftRightButtonFunction(_shiftRightButtonFunction3D);
}

void TYCameraEditor::setToNavigationFree()
{
    setLeftButtonFunction(_leftButtonFunctionFree);
    setMiddleButtonFunction(_middleButtonFunctionFree);
    setRightButtonFunction(_rightButtonFunctionFree);
    setShiftLeftButtonFunction(_shiftLeftButtonFunctionFree);
    setShiftRightButtonFunction(_shiftRightButtonFunctionFree);
}

void TYCameraEditor::cameraRotate()
{
    float xf;
    float yf;

    // do nothing if mouse is still on the same pos
    if ((_currentMousePos[0] == _lastMousePos[0]) && (_currentMousePos[1] == _lastMousePos[1]))
    {
        return;
    }

    // first time we do some preprocessing
    if (_firstCall)
    {
        QSize size = _pInteractor->size();
        double vp[4];
        if (_pCurrentCamera)
        {
            _pCurrentCamera->getViewPort(vp);

            _deltaAzimuth = -20.0 / ((vp[2] - vp[0]) * size.width());
            _deltaElevation = -20.0 / ((vp[3] - vp[1]) * size.height());
        }
    }

    if (_pCurrentCamera)
    {
        xf = (_currentMousePos[0] - _lastMousePos[0]) * _deltaAzimuth * _trackballFactor;
        yf = (_lastMousePos[1] - _currentMousePos[1]) * _deltaElevation * _trackballFactor;

        _pCurrentCamera->azimuth(xf);
        _pCurrentCamera->elevation(yf);
    }

    if (!_firstCall)
    {
    }
    else
    {
        _firstCall = false;
    }

    emit cameraUpdated();

    // update
    _pModeler->updateView();

    // save mouse pos
    _lastMousePos[0] = _currentMousePos[0];
    _lastMousePos[1] = _currentMousePos[1];
}

void TYCameraEditor::cameraTranslate()
{
    double viewFocus[4];
    double motionVector[3];

    // do nothing if mouse is still on the same pos
    if ((_currentMousePos[0] == _lastMousePos[0]) && (_currentMousePos[1] == _lastMousePos[1]))
    {
        return;
    }

    if (_pCurrentCamera)
    {
        if (_pCurrentCamera->m_eCameraType == PERSPECTIVE)
        {
            if ((_currentMousePos[0] - _lastMousePos[0]) > 0)
            {
                _pCurrentCamera->moveLeft();
            }
            if ((_currentMousePos[0] - _lastMousePos[0]) < 0)
            {
                _pCurrentCamera->moveRight();
            }
            if ((_currentMousePos[1] - _lastMousePos[1]) > 0)
            {
                _pCurrentCamera->moveUp();
            }
            if ((_currentMousePos[1] - _lastMousePos[1]) < 0)
            {
                _pCurrentCamera->moveDown();
            }
        }
        else
        {
            NxVec3 ret = OGLCamera::worldToDisplay(_pCurrentCamera->to);
            viewFocus[0] = ret.x;
            viewFocus[1] = ret.y;
            viewFocus[2] = ret.z;
            double focalDepth = viewFocus[2];
            NxVec3 retNew = OGLCamera::displayToWorld(NxVec3(
                float(_pInteractor->width() / 2.0 + (_currentMousePos[0] - _lastMousePos[0])),
                float(_pInteractor->height() / 2.0 - (_currentMousePos[1] - _lastMousePos[1])), focalDepth));

            motionVector[0] = _trackballFactor / 10.0 * (_pCurrentCamera->to.x - retNew.x);
            motionVector[1] = _trackballFactor / 10.0 * (_pCurrentCamera->to.y - retNew.y);
            motionVector[2] = _trackballFactor / 10.0 * (_pCurrentCamera->to.z - retNew.z);

            _pCurrentCamera->to.x += motionVector[0];
            _pCurrentCamera->to.y += motionVector[1];
            _pCurrentCamera->to.z += motionVector[2];

            _pCurrentCamera->from.x += motionVector[0];
            _pCurrentCamera->from.y += motionVector[1];
            _pCurrentCamera->from.z += motionVector[2];
        }
    }

    emit cameraUpdated();

    // update
    _pModeler->updateView();

    // save mouse pos
    _lastMousePos[0] = _currentMousePos[0];
    _lastMousePos[1] = _currentMousePos[1];
}

void TYCameraEditor::cameraTranslateKey()
{
    if (_pCurrentCamera)
    {
        if (_bKeyShift)
        {
            _pCurrentCamera->setDistanceStep(1, 1, 1);
        }
        else
        {
            _pCurrentCamera->setDistanceStep(5, 5, 5);
        }

        _pCurrentCamera->calculateStepVectors();
        if (_bKeyUp)
        {
            _pCurrentCamera->moveFront();
        }
        if (_bKeyDown)
        {
            _pCurrentCamera->moveBack();
        }
        if (_bKeyLeft)
        {
            _pCurrentCamera->moveLeft();
        }
        if (_bKeyRight)
        {
            _pCurrentCamera->moveRight();
        }
        if (_bKeyPageUp)
        {
            _pCurrentCamera->moveUp();
        }
        if (_bKeyPageDown)
        {
            _pCurrentCamera->moveDown();
        }
    }

    emit cameraUpdated();

    // update
    _pModeler->updateView();
}

void TYCameraEditor::cameraZoom()
{
    float yf;

    // do nothing if mouse is still on the same y position
    if (_currentMousePos[1] == _lastMousePos[1])
    {
        return;
    }

    yf = float(_currentMousePos[1] - _lastMousePos[1]) / float(_pInteractor->height()) * _trackballFactor;
    // if yf < 0, we need to be between 0..1
    yf = pow(1.1f, yf);

    if (_pCurrentCamera)
    {
        _pCurrentCamera->zoom(1 / yf);
    }

    emit cameraUpdated();

    // update
    _pModeler->updateView(false); //, _pModeler->getShowGrid());

    // save mouse pos
    _lastMousePos[0] = _currentMousePos[0];
    _lastMousePos[1] = _currentMousePos[1];
}

void TYCameraEditor::cameraRoll()
{
    float angle;

    // do nothing if mouse is still on the same pos
    if ((_currentMousePos[0] == _lastMousePos[0]) && (_currentMousePos[1] == _lastMousePos[1]))
    {
        return;
    }

    if (_pCurrentCamera)
    {
        // first time we do some preprocessing
        if (_firstCall)
        {
            _renCenter[0] = _pCurrentCamera->getCenter(_pInteractor->width(), _pInteractor->height()).x;
            _renCenter[1] = _pCurrentCamera->getCenter(_pInteractor->width(), _pInteractor->height()).y;
            _renCenter[2] = _pCurrentCamera->getCenter(_pInteractor->width(), _pInteractor->height()).z;
            _firstCall = false;
        }

        // calculate the angle in radians and roll the camera
        int diffX1 = _currentMousePos[0] - int(_renCenter[0]);
        int diffY1 = _currentMousePos[1] - int(_renCenter[1]);
        int diffX2 = _lastMousePos[0] - int(_renCenter[0]);
        int diffY2 = _lastMousePos[1] - int(_renCenter[1]);

        double a1 = atan2(double(diffY1), double(diffX1));
        double a2 = atan2(double(diffY2), double(diffX2));
        angle = (a2 - a1) / (2.0 * 3.1415926535) * 360.0 / 10.0 * _trackballFactor;

        _pCurrentCamera->roll(angle);

        emit cameraUpdated();
    }

    // update
    _pModeler->updateView();

    // save mouse pos
    _lastMousePos[0] = _currentMousePos[0];
    _lastMousePos[1] = _currentMousePos[1];
}

void TYCameraEditor::doNothing() {}

void TYCameraEditor::cameraZoneZoom(const float focalPoint2D[3], double factor)
{
    double viewFocus[4];
    double d[3];

    if (_pCurrentCamera)
    {
        d[0] = _pCurrentCamera->from.x - _pCurrentCamera->to.x;
        d[1] = _pCurrentCamera->from.y - _pCurrentCamera->to.y;
        d[2] = _pCurrentCamera->from.z - _pCurrentCamera->to.z;

        NxVec3 ret = OGLCamera::worldToDisplay(_pCurrentCamera->to);
        viewFocus[0] = ret.x;
        viewFocus[1] = ret.y;
        viewFocus[2] = ret.z;
        double focalDepth = viewFocus[2];
        NxVec3 retNew = OGLCamera::displayToWorld(NxVec3(focalPoint2D[0], focalPoint2D[1], focalDepth));

        _pCurrentCamera->to.x = retNew.x;
        _pCurrentCamera->to.y = retNew.y;
        _pCurrentCamera->to.z = retNew.z;

        _pCurrentCamera->from.x = retNew.x + d[0];
        _pCurrentCamera->from.y = retNew.y + d[1];
        _pCurrentCamera->from.z = retNew.z + d[2];

        _pCurrentCamera->zoom(1 / factor);
    }

    emit cameraUpdated();

    // update
    _pModeler->updateView();
}

void TYCameraEditor::cameraStepRotateUp()
{
    if (_pCurrentCamera)
    {
        _pCurrentCamera->elevation(-_rotateStep / 1000);
    }

    emit cameraUpdated();

    // update
    _pModeler->updateView();
}
void TYCameraEditor::cameraStepRotateDown()
{
    if (_pCurrentCamera)
    {
        _pCurrentCamera->elevation(_rotateStep / 1000);
    }

    emit cameraUpdated();

    // update
    _pModeler->updateView();
}
void TYCameraEditor::cameraStepRotateLeft()
{
    if (_pCurrentCamera)
    {
        _pCurrentCamera->azimuth(_rotateStep / 1000);
    }

    emit cameraUpdated();

    // update
    _pModeler->updateView();
}
void TYCameraEditor::cameraStepRotateRight()
{
    _pCurrentCamera->azimuth(-_rotateStep / 1000);

    emit cameraUpdated();

    // update
    _pModeler->updateView();
}
void TYCameraEditor::cameraStepTranslateUp()
{
    NxVec3 motionVector;

    if (_pCurrentCamera)
    {
        motionVector.x = 0;
        motionVector.y = 1;
        motionVector.z = 0;
        motionVector.normalize();
        motionVector *= _translateStep;

        _pCurrentCamera->to.x += motionVector.x;
        _pCurrentCamera->to.y += motionVector.y;
        _pCurrentCamera->to.z += motionVector.z;

        _pCurrentCamera->from.x += motionVector.x;
        _pCurrentCamera->from.y += motionVector.y;
        _pCurrentCamera->from.z += motionVector.z;
    }

    emit cameraUpdated();

    // update
    _pModeler->updateView();
}

void TYCameraEditor::cameraStepTranslateDown()
{
    NxVec3 motionVector;

    if (_pCurrentCamera)
    {
        motionVector.x = 0;
        motionVector.y = 1;
        motionVector.z = 0;
        motionVector.normalize();
        motionVector *= _translateStep;

        _pCurrentCamera->to.x -= motionVector.x;
        _pCurrentCamera->to.y -= motionVector.y;
        _pCurrentCamera->to.z -= motionVector.z;

        _pCurrentCamera->from.x -= motionVector.x;
        _pCurrentCamera->from.y -= motionVector.y;
        _pCurrentCamera->from.z -= motionVector.z;
    }

    emit cameraUpdated();

    // update
    _pModeler->updateView();
}
void TYCameraEditor::cameraStepTranslateLeft()
{
    NxVec3 motionVector;

    if (_pCurrentCamera)
    {
        motionVector.x = _pCurrentCamera->from.x - _pCurrentCamera->to.x;
        motionVector.y = _pCurrentCamera->from.y - _pCurrentCamera->to.y;
        motionVector.z = _pCurrentCamera->from.z - _pCurrentCamera->to.z;
        motionVector = motionVector.cross(NxVec3(0, 1, 0));
        motionVector.normalize();
        motionVector *= _translateStep;

        _pCurrentCamera->to.x += motionVector.x;
        _pCurrentCamera->to.y += motionVector.y;
        _pCurrentCamera->to.z += motionVector.z;

        _pCurrentCamera->from.x += motionVector.x;
        _pCurrentCamera->from.y += motionVector.y;
        _pCurrentCamera->from.z += motionVector.z;
    }

    emit cameraUpdated();

    // update
    _pModeler->updateView();
}
void TYCameraEditor::cameraStepTranslateRight()
{
    NxVec3 motionVector;

    if (_pCurrentCamera)
    {
        motionVector.x = _pCurrentCamera->from.x - _pCurrentCamera->to.x;
        motionVector.y = _pCurrentCamera->from.y - _pCurrentCamera->to.y;
        motionVector.z = _pCurrentCamera->from.z - _pCurrentCamera->to.z;
        motionVector = motionVector.cross(NxVec3(0, 1, 0));
        motionVector.normalize();
        motionVector *= _translateStep;

        _pCurrentCamera->to.x -= motionVector.x;
        _pCurrentCamera->to.y -= motionVector.y;
        _pCurrentCamera->to.z -= motionVector.z;

        _pCurrentCamera->from.x -= motionVector.x;
        _pCurrentCamera->from.y -= motionVector.y;
        _pCurrentCamera->from.z -= motionVector.z;
    }

    emit cameraUpdated();

    // update
    _pModeler->updateView();
}
void TYCameraEditor::cameraStepZoomIn()
{
    if (_pCurrentCamera)
    {
        _pCurrentCamera->zoom(1 / _zoomStep);
    }

    emit cameraUpdated();

    // update
    _pModeler->updateView();
}
void TYCameraEditor::cameraStepZoomOut()
{
    if (_pCurrentCamera)
    {
        _pCurrentCamera->zoom(_zoomStep);
    }

    emit cameraUpdated();

    // update
    _pModeler->updateView();
}
